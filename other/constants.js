exports.domofondFields = {
    "Цена": {
        name: "price",
        repl: (el) => (el.replace(/\D+/g,""))
    },
    "Комнаты": {
        name: "rooms_count",
        repl: (el) => (el)
    },
    "Материалздания": {
        name: "house_material",
        repl: (el) => (el)
    },
    "Этаж": {
        name: "floor",
        repl: (el) => (el.split('/')[0])
    },
    "Площадь": {
        name: "area",
        repl: (el) => ({total: el.replace(/\D+/g,"")})
    },
    "Площадькухни": {
        name: "area",
        repl: (el) => ({kitchen: el.replace(/\D+/g,"")})
    },
    "Жилаяплощадь": {
        name: "area",
        repl: (el) => ({living_space: el.replace(/\D+/g,"")})
    },
    "Номервкаталоге": {
        name: "domofond",
        repl: (el) => ({id: el})
    }
}
