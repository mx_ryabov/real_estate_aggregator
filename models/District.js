var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var crypto = require('crypto');
var async = require('async');
var config = require('../config.js');
var _ = require('lodash');

var debug = require('debug')('app:server');


var schema = new Schema({
    title: {
        type: String,
        required: true
    },
    n1_id: {
        type: Number,
    }
});


exports.District = District = mongoose.model('District', schema);
