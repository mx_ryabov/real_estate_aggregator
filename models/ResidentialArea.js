var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var Flat = require('./Flat.js');

var crypto = require('crypto');
var async = require('async');
var config = require('../config.js');
var _ = require('lodash');

var debug = require('debug')('app:server');


var ResidentialArea = Flat.Flat.discriminator('ResidentialArea', new Schema({
    type: {
        type: String,
        default: "residential"
    },
    rooms_count: {
        type: Number
    },
    district: {
        type: mongoose.Schema.ObjectId,
        ref: 'District',
    },
    flat_type: {
        type: Object
    },
    address: {
        street: {
            type: Object
        },
        house_number: {
            type: String
        }
    },
    area: {
        total: {
            type: Number
        },
        living_space: {
            type: Number
        },
        kitchen: {
            type: Number
        }
    },
    floor: {
        type: Number
    },
    house_material: {
        type: Object
    },
    bathroom: {
        type: Object
    },
    layout_type: {
        type: Object // Планировка
    },
    balcony_count: {
        type: Number
    }
}, {discriminatorKey: 'kind'}));


exports.ResidentialArea = ResidentialArea;
