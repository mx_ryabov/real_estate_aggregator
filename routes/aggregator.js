var express = require('express');
var router = express.Router();
var ResidentialArea = require('../models/ResidentialArea').ResidentialArea;
var CommercialArea = require('../models/СommercialArea').CommercialArea;
var City = require('../models/City').City;
var District = require('../models/District').District;
var request = require('request');
const async = require('async');
var htmlToJson = require('html-to-json');
var domofondFields = require('../other/constants.js').domofondFields;
var cyrillicToTranslit = require('cyrillic-to-translit-js');
const DomofondParser = require('../parsers/domofond');


/* GET home page. */
router.get('/n1/cities', function(req, res, next) {
    var params = new URLSearchParams({
        country_id: 643, //этот id выявлен методом тыка и принадлежит видимо Рашке
        fields: ["id", "params", "name_ru"],
        ranks: "state_capital,state,region_capital",
        limit: 100
    });

    request(
        `http://api.n1.ru/api/v1/offers_geo/cities/?${params.toString()}`,
        (err, res_n1, body) => {
            body = JSON.parse(body);

            body.result.forEach((city) => {
                City.findOne({"n1.id": city.id}).exec((err, foundCity) => {
                    if (!foundCity) {
                        var newCity = new City({
                            title: city.name_ru,
                            n1: {
                                id: city.id,
                                suburbs: city.params.suburbs
                            }
                        });
                        newCity.save();
                    }
                });
            });

            res.json(body)
        }
    );
});

router.get('/n1/flats', function(req, res, next) {
    function addDistrict(flat, resid_flat, callback) {
        if (flat.params.district) {
            District.findOne({n1_id: flat.params.district.id}).exec((err, district) => {
                if (!district) {
                    district = new District({title: flat.params.district.name_ru, n1_id: flat.params.district.id});
                    district.save(callback(err, district));
                } else {
                    callback(null, district);
                }
            });
        } else {
            callback(null, null);
        }

    }

    function addCity(flat, resid_flat, callback) {
        City.findOne({"n1.id": flat.params.city.id}).exec((err, city) => {
            if (!city) {
                city = new City({title: flat.params.city.name_ru, n1: {id: flat.params.city.id} });
                city.save(callback(err, city));
            } else {
                callback(null, city);
            }
        });
    }

    function parseResidentialArea(params, nextCity, cityForLog) {
        request(
            `https://api.n1.ru/api/v1/offers/?${new URLSearchParams(params).toString()}`,
            (err, res_n1, body) => {
                console.log("offset::: ", params.offset);
                try {
                    body = JSON.parse(body);
                } catch (e) {
                    console.log('body::: ', body);
                    console.log('err::: ', e);
                    body = {};
                }

                console.log('errors::: ', body.errors);
                console.log('city for log::: ', cityForLog);
                console.log('rubruc::: ', params["query[0][rubric]"]);
                var resultCount = body.result ? body.result.length : null;
                console.log('result count::: ', resultCount);
                console.log("\n");
                if (!body.errors && resultCount) {
                    /*async.eachSeries(*/body.result.forEach((flat, next) => {
                        ResidentialArea.findOne({n1_id: flat._id}).exec((err, resid_flat) => {
                            if (!resid_flat) {
                                resid_flat = ResidentialArea({n1_id: flat._id});
                                addCity(flat, resid_flat, (err, city) => {
                                    resid_flat.city = city;
                                    resid_flat.deal_type = flat.deal_type;
                                    resid_flat.rooms_count = flat.params.rooms_count;

                                    addDistrict(flat, resid_flat, (err, district) => {
                                        resid_flat.district = district;
                                        resid_flat.flat_type = flat.params.type; //в типе предлагается объект
                                        resid_flat.address.street = flat.params.street;
                                        resid_flat.address.house_number = flat.params.house_number;
                                        resid_flat.area.total = flat.params.total_area;
                                        resid_flat.area.living_space = flat.params.living_area;
                                        resid_flat.area.kitchen = flat.params.kitchen_area;
                                        resid_flat.floor = flat.params.floor;
                                        resid_flat.house_material = flat.params.house_material_type;
                                        resid_flat.bathroom = flat.params.wc_type;
                                        resid_flat.layout_type = flat.params.layout_type;
                                        resid_flat.balcony_count = flat.params.balcony_count;
                                        resid_flat.save(/*err => next()*/);
                                    });
                                });
                            }
                        });
                    });

                    params.offset = params.offset + params.limit;

                    return parseResidentialArea(JSON.parse(JSON.stringify(params)), nextCity, cityForLog);
                } else {
                    nextCity();
                    console.log('============');
                }
            }
        )
    }

    function parseCommercialArea(params, nextCity, cityForLog) {
        request(
            `https://api.n1.ru/api/v1/offers/?${new URLSearchParams(params).toString()}`,
            (err, res_n1, body) => {
                console.log("offset::: ", params.offset);
                try {
                    body = JSON.parse(body);
                } catch (e) {
                    console.log('body::: ', body);
                    console.log('err::: ', e);
                    body = {};
                }

                console.log('errors::: ', body.errors);
                console.log('city for log::: ', cityForLog);
                console.log('rubruc::: ', params["query[0][rubric]"]);
                var resultCount = body.result ? body.result.length : null;
                console.log('result count::: ', resultCount);
                console.log("\n");
                if (!body.errors && resultCount) {
                    async.eachSeries(body.result, (flat, next) => {
                        CommercialArea.findOne({n1_id: flat._id}).exec((err, comm_flat) => {
                            if (!comm_flat) {
                                comm_flat = CommercialArea({n1_id: flat._id});
                                addCity(flat, comm_flat, (err, city) => {
                                    comm_flat.city = city;
                                    comm_flat.deal_type = flat.deal_type;

                                    addDistrict(flat, comm_flat, (err, district) => {
                                        comm_flat.district = district;
                                        comm_flat.address.street = flat.params.street;
                                        comm_flat.address.house_number = flat.params.house_number;
                                        comm_flat.area.from = flat.params.area || flat.params.total_area;
                                        comm_flat.area.to = flat.params.area || flat.params.total_area;
                                        comm_flat.building_type = flat.params.commercial_building_type;
                                        comm_flat.save(err => next());
                                    });
                                });
                            }
                        });
                    });

                    params.offset = params.offset + params.limit;

                    return parseResidentialArea(JSON.parse(JSON.stringify(params)), nextCity, cityForLog);
                } else {
                    nextCity();
                    console.log('============');
                }
            }
        )
    }

    ["flats", "rooms", "cottage", "commercial"].forEach((rubric) => {
        console.log("rubric::: ", rubric);


        City.find({}).exec((err, cities) => {
            async.eachSeries(cities, (city, nextCity) => {
                var params = {
                    limit: 200,
                    offset: 0,
                    "query[0][deal_type]": "sell",
                    status: "published"
                };
                var parseFunction;

                if (rubric === "commercial") {
                    params = Object.assign({
                        "query[0][rubric]": "commercial",
                        "groups[0]": "params.type.value",
                        "groups[1]": "params.purposes.value",
                    }, params);
                    parseFunction = parseCommercialArea;
                } else {
                    params = Object.assign({
                        "query[0][rubric]": rubric,
                        "groups[0]": "params.is_newbuilding",
                    }, params);
                    parseFunction = parseResidentialArea;
                }

                //params = new URLSearchParams(params);

                console.log('============');
                console.log('city:: ', city.title, ', id: ', city.n1.id, ', suburbs count: ', city.n1.suburbs.length);
                params["filter[city_id]"] = [city.n1.id].concat(city.n1.suburbs);


                console.log('params::: ', params.toString());

                parseFunction(params, nextCity, city.title);


            });


        });
    });
    res.json({"mess": "ok"});
});


router.get('/domofond/cities', function(req, res, next) {
    DomofondParser.execGetCities();
    /*request(
        "https://www.domofond.ru/prodazha",
        function(err, res_df, body) {
            var initialSettings;
            try {
                body = body.split("Home.HomeSearchBarControl.renderTo(document.getElementById('")[1];
                body = body.split("),")[1];
                body = body.split(");")[0]
                initialSettings = JSON.parse(body);
            } catch (e) {

            }
            if (initialSettings) {
                var {popularAreas, remainingAreas} = initialSettings.locationSearchBar;
                popularAreas.concat(remainingAreas).forEach((area) => {
                    request(
                        `https://www.domofond.ru/shared/areaselectiondialog?AreaId=${area.areaId}&AreaType=Region`,
                        function(error, res_cts, body_cts) {
                            htmlToJson.parse(body_cts, function() {
                                return this.map('div.b-checkbox.cityItem', (cityItem) => {
                                    return [cityItem.attr("data-id"), cityItem.attr("data-name")];
                                })
                            }).done(function(cities) {

                                cities.forEach((city) => {
                                    console.log(`for ${area.displayName} ${city[0]} and ${city[1]}\n`);
                                    City.findOne({title: city[1]}).exec((err, foundCity) => {
                                        if (!foundCity) {
                                            foundCity = new City({title: city[1]});
                                        }
                                        foundCity.domofond.id = city[0];
                                        foundCity.domofond.latinName = cyrillicToTranslit().transform(city[1], "_").toLowerCase();
                                        foundCity.domofond.region.titleLatin = area.latinName;
                                        foundCity.domofond.region.id = area.areaId;
                                        foundCity.save();
                                    });
                                })
                            }, function(parseError) {
                                console.log(parseError);
                            });
                        }
                    );
                });
            }

        }
    )*/
    res.json({mess: "ok"});

});

router.get('/domofond/flats', function(req, res, next) {
    function parseOneFlat(flatUrl) {
        request(
            flatUrl,
            //`https://www.domofond.ru/3-komnatnaya-kvartira-na-prodazhu-moskva-253935620`,
            function(err, res_df, body) {
                htmlToJson.parse(body, function() {
                    return this.map('.b-details-table .e-table-column li', (item) => {

                        let keyVal = item.text().replace(/\s/g,'').split(":");
                        console.log(keyVal[0]);
                        let domField = domofondFields[keyVal[0].split("(")[0]];
                        if (domField) return [domField.name, domField.repl(keyVal[1])]
                    });
                }).done(function(items) {
                    let resObj = {};
                    items.forEach((field) => {
                        if (field) {
                            if(resObj[field[0]]) {
                                let key = Object.keys(field[1])[0]
                                resObj[field[0]][key] = field[1][key];
                            } else {
                                resObj[field[0]] = field[1];
                            }
                        }
                    });
                    htmlToJson.parse(body, function() {
                        return this.map('a[itemprop="address"]', item => {
                              return item.text()
                        })
                    }).done(items => {
                        resObj.address = {street: {fullAddress: items[0]} };
                        console.log(resObj);
                    }, err => {})

                }, function(err) {
                    console.log("err::: ", err);
                });
            }
        );
    }

    function getPageCount(bodyFirstPage, type, city) {
        htmlToJson.parse(bodyFirstPage, function() {
            return this.map('.e-pages li a', numPages => {
                return numPages.text();
            });
        }).done(nums => {
            //console.log(`last num page: ${nums.pop()}`);
            getFlatsCountOnPage(bodyFirstPage, nums.pop(), type, city);
        })
    }

    function getFlatsCountOnPage(bodyPage, countPages, type, city) {
        //b-results-tile js_listingTileContainer b-display-listing-tile
        htmlToJson.parse(bodyPage, function() {
            return this.map('.b-results-tile.js_listingTileContainer.b-display-listing-tile', flats => {
                return flats.text();
            });
        }).done(flats => {
            //console.log(`flats count on page: ${flats.length}`);
            if (flats.length > 0) getFlatsOnAllCityPages(bodyPage, countPages || 1, type, city);

        })
    }

    function getFlatsOnAllCityPages(bodyPage, countPages, type, city) {
        for (var i = 1; i <= countPages; i++) {
            request(
                `https://www.domofond.ru/prodazha-${type}-${city.domofond.latinName}-c${city.domofond.id}?Page=${i}`,
                function(err, res, body) {
                    console.log('errorororor:: ', err);
                    if (!err) {
                        htmlToJson.parse(body, function() {
                            return this.map('.b-results-tile.js_listingTileContainer.b-display-listing-tile a', flats => {
                                return flats.attr("href");
                            });
                        }).done(flatHrefs => {
                            flatHrefs.forEach(href => {
                                parseOneFlat(`https://www.domofond.ru${href}`);
                            })

                        })
                    }

                }
            )
        }
    }

    ["kvartiry"/*, "komnaty", "doma", "kommercheskay-nedvizhimost"*/].forEach((type) => {
        City.find({"domofond.id": {$ne: null}}).exec((err, cities) => {
            cities.forEach((city) => {
                request(
                    `https://www.domofond.ru/prodazha-${type}-${city.domofond.latinName}-c${city.domofond.id}`,
                    function(err, res_df, body) {
                        console.log(err);
                        try {
                            if (!err) getPageCount(body, type, city);
                        } catch (e) {
                            console.log(body);
                        }

                    }
                );
            });
        });
    });
    /*request(
        `https://www.domofond.ru/prodazha-kvartiry-bashkortostan-r41`,
        function(err, res_df, body) {
            console.log(body.indexOf("Стерлитамак"));
        }
    );*/
    res.json({})

});

module.exports = router;
