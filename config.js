module.exports = {
    mongoUrl: "mongodb://localhost/aggregator",
    session: {
        secret: 'Tf64576ygvH5fr%$%^&uh',
        name: 'aggregator.sid',
        proxy: true,
        resave: true,
        saveUninitialized: true
    },
    appSalt: "hb^hbfG^4F5tfgG56tgfr&8y6tgNmj",
}
